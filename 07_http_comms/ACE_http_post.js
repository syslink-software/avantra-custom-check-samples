
// Name: HTTP Post
// Type: Custom check example-code ONLY (please adapt to your environment)
// Author: Brenton O'Callaghan (Avantra)
// Date: 27th April 2021
// Description: A very simple custom check that posts data to a URL.
//
// Considerations: None
//
// Version Log:
//              1.0 - first release

const ccName = "ACE_http_post"; // ace = Avantra, Custom Check, Example
const ccVersion = 1.0;

// This is a sample URL that provides a JSON service. This worked at the 
// time of writing and is not affiliated with Avantra. We recommend you use
// your own sample service.
var sampleJSONService = 'https://jsonplaceholder.typicode.com/todos/';

var dataToPost = '{"sample":"data"}';

// Build the body that you intend to post to the server.
var body = requestBodyBuilder.mediaType("application/json; charset=UTF-8").body(dataToPost).build();

// Build the request object and call the "get" method.
var request = requestBuilder
              .url(sampleJSONService)
              .post(body)
              .header('User-Agent', 'avantra-custom-check')
              .build();

// Execute the call to the server.
var response;
try {
    response = http.newCall(request).execute();
} catch (error) {
    check.message = "Unable to execute the HTTP request to the URL: " + sampleJSONService;
}

if (response !== undefined) {
    var resultMessage = "The http response code from the server was: " + response.code() + ":\n\n";

    // If the response is successful.
    if (response.isSuccessful()) {
        // Parse the response.
        var jsonObject = JSON.parse(response.body().string());

        // Do something interesting with the object 
        // ...
        // ....
        
        // Set the response code as the message output.
        check.message = resultMessage + JSON.stringify(jsonObject);
    } else {
        // Set the response code as the message output.
        check.message = resultMessage;
    }
}