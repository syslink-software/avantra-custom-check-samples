# HTTP Communications - Sample List #

This is the list of sample code provided in this section. This section is designed to showcase how to interact with remote REST based web hosts using the supported verbs.

Custom-check Name | Description | Author | Output
:---:| --- | --- | ---
[`ACE_http_get.js`](ACE_http_get.js) | Perform a GET on a web address | Avantra Team | ![](./ACE_http_get.png)
[`ACE_http_post.js`](./ACE_http_post.js) | Perform a POST to a web address | Avantra Team | ![](./ACE_http_post.png)