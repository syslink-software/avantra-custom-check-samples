// Name: Simple output example
// Type: Custom check example-code ONLY (please adapt to your environment)
// Author: Brenton O'Callaghan (Avantra)
// Date: 23rd April 2021
// Description: A very simple custom check that returns a text-based result.
//
// Considerations: None
//
// Version Log:
//              1.0 - first release

const ccName = "ACE_outputFormatting_simple"; // ace = Avantra, Custom Check, Example
const ccVersion = 1.0;

function okResult(){
    check.message = "Status: OK\n\nThis is the status text for the OK result.";
    check.status = OK;
}

function warningResult(){
    check.message = "Status: WARNING\n\nThis is the status text for the WARNING result.";
    check.status = WARNING;
}

function criticalResult(){
    check.message = "Status: CRITICAL\n\nThis is the status text for the CRTICIAL result.";
    check.status = CRITICAL;
}

// Uncomment only the example you want to use (1 at a time)
okResult();
//warningResult();
//criticalResult();