// Name: Table output example
// Type: Custom check example-code ONLY (please adapt to your environment)
// Author: Brenton O'Callaghan (Avantra)
// Date: 23rd April 2021
// Description: A very simple custom check that returns a table-based result.
//
// Considerations: None
//
// Version Log:
//              1.0 - first release

const ccName = "ACE_outputFormatting_table"; // ace = Avantra, Custom Check, Example
const ccVersion = 1.0;

/*
 *  The function to build the headers of our table.
 */
function buildTableHeaders(){
    check.addTableColumn('Title');
    check.addTableColumn('Description');
    check.addTableColumn('Status');
}

var sampleData = [
    {
        "title":"sample 1",
        "description":"This is the first sample",
        "status" : 0
    },
    {
        "title":"sample 2",
        "description":"This is the second sample in warning status",
        "status" : 1
    },
    {
        "title":"sample 1",
        "description":"This is the final sample in critical status",
        "status" : 2
    }
];

// Setup the output table.
buildTableHeaders();

// Populate the table with the data.
for (const item in sampleData) {
    // Always create a new row before inserting Data.
    check.newRow();
    check.addCell(sampleData[item].title);
    check.addCell(sampleData[item].description);

    // We can add useful color coding to the cell entries.
    // First we determine which color to use based on the data provided.
    var statusColor;
    switch (sampleData[item].status) {
        case 0:
            statusColor = OK;
            break;
        case 1:
            statusColor = WARNING;
            break;
        case 2:
            statusColor = CRITICAL;
            break;
        default:
            break;
    }
    // Now we create the cell entry with the color coding in place.
    check.addCell("Status Code " + sampleData[item].status, statusColor);
}

// Don't forget to set the overall check status and description text.
check.message = "The following entries have been processed by this check:";
check.status = CRITICAL;