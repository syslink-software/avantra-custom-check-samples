# Output Formatting - Sample List #

This is the list of sample code provided in this section. This section is designed to showcase some ways to output data to the Avantra UI from a RUN_JS custom check.

Custom-check Name | Description | Author | Output
:---:| --- | --- | ---
[`ACE_outputFormatting_simple.js`](ACE_outputFormatting_simple.js) | Simple check result output | Avantra Team | ![](./ACE_outputFormatting_simple.png)
[`ACE_outputFormatting_table.js`](ACE_outputFormatting_table.js) | Table check result output | Avantra Team | ![](./ACE_outputFormatting_table.png)