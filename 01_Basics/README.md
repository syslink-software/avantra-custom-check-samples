# Basics - Sample List #

This is the list of sample code provided in this section. This section is designed to introduce you to the basics of writing a custom check RUN_JS check in Avantra. Make sure you are familiar with these concepts to write the best custom checks.

Custom-check Name | Description | Author | Output
:---:| ---| --- | ---
[`ACE_basics_helloWorld.js`](./ACE_basics_helloWorld.js) | `Hello World` custom check designed to show you the basics of how to write a RUN_JS custom check. | Avantra Team |  ![](./ACE_basics_helloWorld.png)
[`ACE_basics_executeOSCommand.js`](./ACE_basics_executeOSCommand.js) | Execute the OS level command `ls` on the folder `/opt` | Avantra Team  |  ![](./ACE_basics_executeOSCommand.png) 
[`ACE_basics_debugging.js`](./ACE_basics_debugging.js) | Learn to debug your scripts as you write them. This custom check shows how to print debug statements during script execution | Avantra Team  |  ![](./ACE_basics_debugging.png)
[`ACE_basics_localStorage.js`](./ACE_basics_localStorage.js) | Learn how to use localStorage which allows you to store data in between check exections | Avantra Team  |  ![](./ACE_basics_localStorage.png)