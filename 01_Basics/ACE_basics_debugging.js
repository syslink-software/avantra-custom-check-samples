// Name: Debugging
// Type: Custom check example-code ONLY (please adapt to your environment)
// Author: Brenton O'Callaghan (Avantra)
// Date: 20th April 2021
// Description: A very simple custom check that shows you how to debug your run_js code
//
// Considerations: None
//
// Version Log:
//              1.0 - first release

const ccName = "ACE_basics_debugging"; // ace = Avantra, Custom Check, Example
const ccVersion = 1.0;

// Set this flag to false to stop outputting debug statements.
const debuggingEnabled = true;

// We use this function to determine whether to output
// debug messages or not. This is really useful when using longer
// scripts where you may want to keep debugging statements in place
// but not have them actually output to the console all the time.
function debugLog(message){
    if(debuggingEnabled){
        print(message);
    }
}

// For the purporses of this demo I'm using a complex object to store the 
// result data. This is to we can see how "JSON.stringify()" works later.
var result = {
    "outputMessage": "blah blah blah",
    "outputCode": OK
}

// Do a debug log out with a string representation of the "result" object.
debugLog("Just prior to setting the output message to " + JSON.stringify(result));
check.message = result.outputMessage;
debugLog("Just after setting the output message");
debugLog("Just before setting the output status");
check.status = result.outputCode;
debugLog("Just after setting the output status");
debugLog("All done");