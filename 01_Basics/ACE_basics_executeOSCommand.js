// Name: Execute OS Command
// Type: Custom check example-code ONLY (please adapt to your environment)
// Author: Brenton O'Callaghan (Avantra)
// Date: 20th April 2021
// Description: A very simple custom check that executes a command on the server host OS.
//
// Considerations:
//                  - Valid for Unix-like operating systems
//
// Version Log:
//              1.0 - first release

const ccName = "ACE_basics_executeOSCommand"; // ace = Avantra, Custom Check, Example
const ccVersion = 1.0;

// Function to execute an ls command on the underlying unix OS.
function ls(directory_path){
    return os.exec("sh -c 'ls -al " + directory_path + "'" );
}

var ls_result = ls('/opt');

// Determine whether the command executed successfully and set the check
// result to be displayed in Avantra. We also set the beginning of the result
// text to be displayed along side the check result.
if(ls_result.exitCode === 0){
    check.status = OK;
    check.message = "The content of directory /opt is:\n";
} else {
    check.status = WARNING;
    check.message = "Command failed!\n";
}

// Finally we add the command text output (whether it is an error or valid)
check.message = check.message + ls_result.out;