// Name: Hello World
// Type: Custom check example-code ONLY (please adapt to your environment)
// Author: Brenton O'Callaghan (Avantra)
// Date: 20th April 2021
// Description: A very simple custom check that return OK result with the text "hello World"
//
// Considerations: None
//
// Version Log:
//              1.0 - first release

const ccName = "ACE_basics_helloWorld"; // ace = Avantra, Custom Check, Example
const ccVersion = 1.0;

function sayHello(){
    check.message = "My first Avantra custom-check - Hello World!!";
    check.status = OK;
}

sayHello();