// Name: Local Storage example
// Type: Custom check example-code ONLY (please adapt to your environment)
// Author: Brenton O'Callaghan (Avantra)
// Date: 20th April 2021
// Description: It is possible to store data between check runs of the same check. This is known as "local storage".
//              This check illustrates how to use this function.
//
// Considerations: None
//
// Version Log:
//              1.0 - first release

const ccName = "ACE_basics_localStorage"; // ace = Avantra, Custom Check, Example
const ccVersion = 1.0;

const persistent_counter_name = "persistent_counter";

// get the last stored value of the counter and parse it as an Int so we can easily increment it.
// The second argument (marked as zero (0) here) means that if there is no value found, we default to 0.
var localStorageCounter = parseInt(localStorage.getItem(persistent_counter_name, 0));

// Incremenent the counter and store it back into local storage for the next run.
localStorageCounter ++;
localStorage.setItem(persistent_counter_name, localStorageCounter);

// Output the current counter value to the check result.
check.message = "The persisted counter is now at: " + localStorageCounter;