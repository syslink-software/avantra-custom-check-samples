// Name: Execute select command on the database
// Type: Custom check example-code ONLY (please adapt to your environment)
// Author: Brenton O'Callaghan
// Date: 29th April 2021
// Description: A custom check that executes an SQL statement on the underlying monitored DB.
//
// Considerations:
//                  - Valid for database systems
//
// Version Log:
//              1.0 - first release

const ccName = "ACE_db_executeCommand"; // ace = Avantra, Custom Check, Example
const ccVersion = 1.0;

/*
 *  The function to build the headers of our results table.
 */
function buildTableHeaders(){
    check.addTableColumn('Username');
    check.addTableColumn('Date');
}

var result = database.execute('SELECT USER as U, today(*) as T FROM SYS.DUMMY;');

// get the result rows from the query.
var resultSet = result.getRows();

// Loop through the result set and output the results.
var counter = 0;
for (counter = 0; counter < resultSet.length; counter++) {
    check.newRow();
    check.addCell(resultSet[counter].getValue("U"));
    check.addCell(resultSet[counter].getValue("T"));
}

check.message = "Execution successful with " + resultSet.length + " retrieved rows: ";