# Basics - Sample List #

This is the list of sample code provided in this section. This section is designed to introduce you to the basics of writing a custom check RUN_JS check in Avantra. Make sure you are familiar with these concepts to write the best custom checks.

Custom-check Name | Description | Author | Output
:---:| --- | --- | ---
[`ACE_server_journald.js`](ACE_server_journald.js) | Monitor for errors within a service using journald | Avantra Team | ![](./ACE_server_journald.png)