// Name: JournalD process error monitor
// Type: Custom check example-code ONLY (please adapt to your environment)
// Author: Brenton O'Callaghan (Avantra)
// Date: 23rd March 2021
// Description: An example custom check to monitor a systemd service for errors over the last
//              24 hours.
//
// Considerations:
//              The service must exist in the system e.g. systemctl status <serviceName> must work.
//              The user must have permissions to execute the journalctl command 
//
// Version Log:
//              1.0 - first release
//              1.1 - adding error output for when the command fails (usually when insufficient permissions)


const cc_name = "ACE_server_journaldErrorMonitor"; // ace = Avantra, Custom Check, Example
const cc_version = 1.0;

const serviceName = "google-oslogin-cache";

function convertUNIXTimeToString(unixTimestamp){
    var a = new Date(Number(unixTimestamp) / 1000);
    const months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    var time = a.getDate() + ' ' + months[a.getMonth()] + ' ' + a.getFullYear() + ' - ' + a.getHours() + ':' + a.getMinutes() + ':' + a.getSeconds() ;
    return time;
}

function getTodayWithTime(time){
    var a = new Date();
    var month = a.getMonth() + 1;
    var day = a.getDate();
    if(month < 10){
        month = "0" + month;
    }
    if(day < 10){
        day = "0" + day;
    }
    var result = "" + a.getFullYear() + '-' + month + '-' + day + ' ' + time;
    return result;
}

// Function to execute the journalctl command for a specific process.
function journalctl(unitName, priority, sinceString, untilString) {

    // Construct the command to be executed.
    // -u = unit name aka service name
    // -p = the priority (or higher) of the message e.g. err (error), emerg (emergency), info etc.
    // -q = quiet mode (i.e. don't output anything if there are no messages)
    // --output=json-pretty = formatting the output as JSON so we can parse it easier here.
    // --since = the timestamp after which messages should be considered e.g. --since="2021-04-21 00:00:00"
    // --until = the timestamp before which messages should be considered e.g. --until="2021-04-21 23:59:59"
    var journalctlCommand = 'sudo journalctl -u ' + unitName +
        ' -p ' + priority +
        ' -q ' +
        ' --output=json ' +
        ' --since="' + sinceString +
        '" --until="' + untilString + '"';

    return os.exec(journalctlCommand);
}

// Execute the command to check for errors on our chosen service.
var journalctl_result = journalctl(serviceName, "err", getTodayWithTime("00:00:00"), getTodayWithTime("23:59:59"));

// Determine whether the command executed successfully and set the check
// result to be displayed in Avantra.
if(journalctl_result.exitCode === 0){
    // Process the output to see if there are any errors that require attention.

    // If the length of the output is > 0 then there are messages (and so, errors) so we report critical.
    if(journalctl_result.out.length > 0){
        check.status = CRITICAL;

        try {
            // Regex to help us format the command output JSON into an array of JSON objects.
            const regex = /(\}.\{)/sg;

            // Inelegant but it works - add square brackets to suorround the JSON objects to create an array object.
            var structuredJsonString = "[" + journalctl_result.out.replace(regex, '},{') + "]";

            // Create a javascript object from the string object.
            var resultObject = JSON.parse(structuredJsonString);

            // prepare result table
            check.addTableColumn('Timestamp');           // __REALTIME_TIMESTAMP
            check.addTableColumn('Host');                // _HOSTNAME
            check.addTableColumn('Priority');            // PRIORITY
            check.addTableColumn('UID of owner');                 // UID of the user that caused the error
            check.addTableColumn('Message');             // MESSAGE

            var counter = 0;
            for (counter =0 ; counter < resultObject.length; counter++) {
                const element = resultObject[counter];
                check.newRow();
                check.addCell(convertUNIXTimeToString(element["__REALTIME_TIMESTAMP"]));
                check.addCell(element["_HOSTNAME"]);
                check.addCell(element["PRIORITY"]);
                check.addCell(element["_UID"]);
                check.addCell(element["MESSAGE"]);
            }
            check.message = 'JournalD reports errors for service "' + serviceName + '"';
        } catch (error) {
            check.message = 'JournalD reports the following errors for service "' + serviceName + '".\n' + journalctl_result.out + '\n\n\n Note: This output is unformatted owning to an error parsing the output.';
        }
    } else {
        check.status = OK;
        check.message = 'No errors reported for service "' + serviceName + '"';
    }
} else {
    check.status = WARNING;
    check.message = 'Unable to retrieve the audit logs for the service "' + serviceName + '".\n' + journalctl_result.out;
}