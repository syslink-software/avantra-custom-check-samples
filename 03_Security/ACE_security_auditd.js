// Name: Audit system monitor
// Type: Custom check example-code ONLY (please adapt to your environment)
// Author: Brenton O'Callaghan (Avantra)
// Date: 23rd March 2021
// Description: An example custom check to monitor for entries in the audit subsystem
//              in linux typically used in hardened to monitor for read/write/execute
//              access to files/folders etc.
//
// Considerations:
//              0) Designed to use a monitoring parameter (cust.security-audit-monitor-key) to know what audit log to use. 
//                  See here for more info
//                  https://docs.avantra.com/product-guide/20.11/avantra/extend-monitoring.html#defining-custom-monitoring-parameters
//              1) Assumes the user running the agent (and so this script) has been added to a group e.g. "auditors"
//                 this group has then been configured in the /etc/audit/auditd.conf config under the key log_group. For example:
//                      log_group = auditors
//
//              2)  For the purposes of this example, there is a file at the path /home/bocallaghan/secret.txt
//                  This file has been added to the audit subsystem by adding an entry to /etc/audit/rules.d/audit.rules
//                  (this file is present in RHEL8 - it may be different in other distros but the syntax is the same)
//                  The sample rule is:
//                                          -w /home/bocallaghan/secret.txt -p rwx -k super_secret_stuff
//                  This rule watches the file (-w) at path /home/bocallaghan/secret.txt under the permissions read/write/execute
//                  Any audit logs will be tagged with the key (-k) super_secret_stuff. This key "super_secret_stuff" should be placed
//                  in the custom monitoring parameter as mentioned in consideration 0 above.
//
// Version Log:
//              1.0 - first release


const cc_name = "ACE_security_auditMonitor"; // ace = Avantra, Custom Check, Example
const cc_version = 1.0;

// Get the monitoring parameter value from Avantra.
var monitoringParameter = monitoredSystem.getMoniParam("cust.security-audit-monitor-key");

// Sanatize the entry to prevent abuse based on a deliberately malformed parameter.
// After all, this value will be passed and executed on the command line.
var audit_search_key = monitoringParameter.replace(/[^_a-zA-Z ]/g, "");

// Function to execute the ausearch command on the underlying unix OS.
function ausearch(key){
    // Execute the ausearch command to search the audit logs. This will fail if you have not completed consideration #1 above.
    return os.exec('ausearch -k "' + audit_search_key + '" --start today --end today --format interpolate  --input-logs');
}

// Perform a regex search on the provided string and return the 1st match
// Allows for elegant returning of "Unknown" if not found.
function regexSearch(string, regex){
    var result = regex.exec(string);
    if(result === null || result.length < 2){
        return "Unknown";
    } else {
        return result[1];
    } 
}

// This function takes the output from ausearch and splits the main info into an array of structured
// objects. This is useful later during output construction.
function parseAuditLogsIntoStructuredArray(logs){

    // Split into array using the ---- which is the standard separator
    var logsArray = logs.split("----");
    // reverse the array so we look at the latest logs first.
    logsArray.reverse();
    var finalResult = [];
    var counter = 0;

    // Loop through and ignore irrelevant entries.
    for (counter = 0; counter < logsArray.length; counter++) {
        var logEntry = logsArray[counter];
        var logEntryArray = logEntry.split("\n");   // We split at the newline char as each log entry is made up of multiple lines.
        var logEntryCounter = 0;
        var logEntryResult = {
            "timestamp": null,  // type=SYSCALL with the value of msg=?
            "username" : null,  // type=SYSCALL with the value of uid=?
            "command" : null,   // type=SYSCALL with the value of exe=?
            "file": null,               // latest type=PATH with the values of name=?
            "workingDirectory": "",     // combination of type=CWD with the values of cwd=?
            "successful" : null         // type=SYSCALL with the value of success=?
        }
        for (logEntryCounter = 0; logEntryCounter < logEntryArray.length; logEntryCounter++) {
            var logEntryLine = logEntryArray[logEntryCounter];

            // Get the type of entry for the current line
            var regex_logEntryType = /(?:^type=)(.+)(?=\smsg=.+)/g;
            var currentLogType = regexSearch(logEntryLine, regex_logEntryType);

            // Process the entry based on the type, as determined above.
            switch (currentLogType) {
                case "PATH":
                    // We only read the "topmost" path entry.
                    if(logEntryResult.file === null){
                        var regex_path = /(?: name=)(.+)(?= inode=.+)/g;
                        logEntryResult.file = regexSearch(logEntryLine, regex_path);
                    }
                    break;
                case "CWD":
                    var regex_path = /(?: cwd=)(.+)(?=$)/g;
                    logEntryResult.workingDirectory = regexSearch(logEntryLine, regex_path);// These lines are always read in reverse order to make up the path.
                    break;
                case "SYSCALL":
                    var regex_timestamp = /(?:msg=audit\()(.+)(?=\.\d+:\d+\).+)/g;
                    var regex_exe = /(?:exe=)(.+)(?= key=.+)/g;
                    var regex_uid = /(?: uid=)(.+)(?= gid=.+)/g;
                    var regex_success = /(?: success=)(.+)(?= exit=.+)/g;
                    logEntryResult.timestamp = regexSearch(logEntryLine, regex_timestamp);
                    logEntryResult.command = regexSearch(logEntryLine, regex_exe);
                    logEntryResult.username = regexSearch(logEntryLine, regex_uid);
                    logEntryResult.successful = regexSearch(logEntryLine, regex_success);
                    break;
                default:
                    break;
            }            
        }
        // If we've retrieved nothing useful - we ignore this entry.
        // Otherwise we put it in the result array.
        if(!(logEntryResult.timestamp === null || logEntryResult.timestamp == "Unknown")){
            finalResult.push(logEntryResult);
        } 
    }
    // Return the valid entries.
    return finalResult;
}

// The main function to execute for this check.
function executeCheck(){

    // Execute the audit log search command.
    var ausearch_result = ausearch(audit_search_key);

    // Determine whether the command executed successfully and set the check
    // result to be displayed in Avantra.
    // 0 = successful execution with audit entries found (CRITICAL)
    // 1 = successful execution with no entries found (OK)
    // <other> = Execution failed (WARNING)
    if(ausearch_result.exitCode === 0){
        check.status = CRITICAL;
        check.message = 'Audit events detected for key "' + audit_search_key + '":\n';
        var result = parseAuditLogsIntoStructuredArray(ausearch_result.out);
        
        check.addTableColumn("Timestamp");
        check.addTableColumn("User");
        check.addTableColumn("Command");
        check.addTableColumn("File");
        check.addTableColumn("Working Directory");
        check.addTableColumn("Success");

        for (const key in result) {
            check.newRow();
            check.addCell(result[key].timestamp);
            check.addCell(result[key].username);
            check.addCell(result[key].command);
            check.addCell(result[key].file);
            check.addCell(result[key].workingDirectory);
            check.addCell(result[key].successful);
        }

    } else if(ausearch_result.exitCode === 1){
        check.status = OK;
        check.message = 'No audit entries detected for key "' + audit_search_key + '"';
    } else {
        check.status = WARNING;
        check.message = "Command 'ausearch' failed to execute successfully:\n\t" + ausearch_result.out;
    }
}

// If we don't have a audit key to use we should not continue.
if(audit_search_key === undefined || audit_search_key === null || audit_search_key === ""){
    check.status = WARNING;
    check.message = "Parameter cust.security-audit-monitor-key has not been set in this system.";
} else {
    // Start the custom check.
    executeCheck();
}