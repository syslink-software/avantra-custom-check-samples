// Name: OpenSSL Version checker
// Type: Custom check example-code ONLY (please adapt to your environment)
// Author: Brenton O'Callaghan (Avantra)
// Date: 23rd March 2021
// Description: An example custom check to verify the current OpenSSL version
//              against the publically available latest version on the GIT repo.
//              This sample is provided without liability, warranty or support.
//              The command used in the OS outputs similar to the following
//                  e.g. from GCP SUSE      -> 'OpenSSL 1.1.1g FIPS  21 Apr 2020'
//                  e.g. from AWS Linux2    -> 'OpenSSL 1.0.2k-fips  26 Jan 2017'
//
// Considerations:
//              This CC assumes that the agent on the server has the ability to communicate
//              With the internet and API endpoint in Github (this may not be true)
//              If you do NOT have internet access, you can rewrite the function called 
//              getlatestOpenSSLVersion() to return a hard coded JSON object similar to:
//
//              function getlatestOpenSSLVersion(openssl_major_version) {
//                  return {
//                      "openssl_major_version": "1_1_1",
//                      "latest_openssl_version_letter" : "k",
//                      "latest_openssl_version_git_tag": "OpenSSL_1_1_1k"
//                  }
//              }
//
// Version Log:
//              1.0 - first release
//              1.1 - Accounting for when no remote version lists are found.


const cc_name = "ACE_security_OpensslVersionChecker"; // ace = Avantra, Custom Check, Example
const cc_version = 1.1;
const userAgent = "application/avantra-cc";   // A user agent must be passed to github for API requests.
const openssl_public_repo = "https://api.github.com/repos/openssl/openssl/tags?per_page=100"; // The API endpoint to search the release tags.

/**
 * Retrieve the latest OpenSSL version from the public git repository for OpenSSL.
 * @param  {string} openssl_major_version The major version in use for which we need the latest letter version.
 * @return {object} A JSON object containing the current major version, latest letter version and the tag of the release in the git repository.
 */
function getlatestOpenSSLVersion(openssl_major_version) {

    // Ensure that if the version number comes in as 1.1.1 we reformat to the git-compatible 1_1_1
    // This is because the tags in the git repository use underscores rather than dots.
    var reformatted_major_version = openssl_major_version.replace(/\./g, "_").toLowerCase();

    // Build the request to the github API.
    var request = requestBuilder.url(openssl_public_repo).get()
                                .addHeader('User-Agent', userAgent)
                                .build();
    var response = http.newCall(request).execute();
    if (response.isSuccessful()) {

        var tagData = JSON.parse(response.body().string()),
            c_tag = 0,
            latestVersion = '',
            latestVersion_gitTag;

        for (c_tag = 0; c_tag < tagData.length; c_tag = c_tag + 1) {

            // Check that this tag matches the release format we're looking for
            // e.g. we want OpenSSL_1_1_1i and not openssl-3.0.0-alpha2
            var tagLength = tagData[c_tag].name.length;
            var tagProduct = tagData[c_tag].name.substring(0, 13).toLowerCase();
            var tagVersion = tagData[c_tag].name.slice(-1).toLowerCase();
    
            if (tagLength === 14 && tagProduct == ("openssl_" + reformatted_major_version)) {
                if (tagVersion > latestVersion) {
                    latestVersion = tagVersion;
                    latestVersion_gitTag = tagData[c_tag].name;
                }
            }
        }
        if(c_tag === 0 && latestVersion == ''){
            return undefined;
        }
        return {
            "openssl_major_version": openssl_major_version,
            "latest_openssl_version_letter" : latestVersion,
            "latest_openssl_version_git_tag": latestVersion_gitTag
        }
    } else {
        return undefined;
    }
}

/**
 * Sets the text output message for the check result (visible in the Avantra UI)
 * @param  {string} output_message The mesage to be displayed as part of the check result.
 */
function setOutputMessage(output_message){
    check.message = output_message;
}

// Get the OpenSSL version info from the OS command and verify we could execute it successfully.
var os_command_output = os.exec("openssl version");
var os_current_openssl_version_text = os_command_output.out;
if(os_command_output.exitCode === 0 && (os_current_openssl_version_text === undefined || os_current_openssl_version_text === null || os_current_openssl_version_text.length < 5)){
    setOutputMessage("Unable to retrieve the openssl version " + os_current_openssl_version_text);
    check.status = WARNING;
} else {

    // Parse the OpenSSL version from the OS and use the major version in use to query the published latest version.
    var os_current_openssl_major_version = os_current_openssl_version_text.substring(8, 13).replace(/\./g, "_");
    var os_current_openssl_version_letter = os_current_openssl_version_text.substring(13, 14);
    var latest_available_version_info = getlatestOpenSSLVersion(os_current_openssl_major_version);

    // If we can't get the latest release info we trigger a warning.
    if(latest_available_version_info === undefined){
        // we couldn't get info on the latest version - send a warning.
        setOutputMessage("Unable to check latest version for the current major version " + os_current_openssl_version_text);
        check.status = WARNING;

    // If we have the OS version info and the latest publication info, we do the processing and setup the check result.
    } else {

        check.addTableColumn("Location");
        check.addTableColumn("Major Version");
        check.addTableColumn("Letter Version");
        check.addTableColumn("Release Tag");

        // Add the latest release information we have (for comparison in the Avantra UI)
        check.newRow();
        check.addCell("Latest");
        check.addCell(latest_available_version_info.openssl_major_version);
        check.addCell(latest_available_version_info.latest_openssl_version_letter, OK);
        check.addCell(latest_available_version_info.latest_openssl_version_git_tag);

        // Add the current server version information we have (for comparison in the Avantra UI)
        check.newRow();
        check.addCell("This server");
        check.addCell(os_current_openssl_major_version);
        
        // Verify if the OS version matches the latest version published.
        if(os_current_openssl_version_letter != latest_available_version_info.latest_openssl_version_letter){
            check.addCell(os_current_openssl_version_letter, CRITICAL);
            setOutputMessage("The server version of OpenSSL " + os_current_openssl_major_version + "(" + os_current_openssl_version_letter + ") does not match the latest known version " + latest_available_version_info.openssl_major_version +"(" + latest_available_version_info.latest_openssl_version_letter + ")");
            check.status = CRITICAL;
        } else {
            check.addCell(os_current_openssl_version_letter, OK);
            setOutputMessage("The server version of OpenSSL " + os_current_openssl_major_version + "(" + os_current_openssl_version_letter + ") appears to be at the latest version " + latest_available_version_info.openssl_major_version +"(" + latest_available_version_info.latest_openssl_version_letter + ")");
            check.status = OK;
        }
        check.addCell(" - - ");
    }
}