# Security - Sample List #

This is the list of sample code provided in this section. This section is designed to showcase some ways to check security elements of various systems using the `os.execute` option. Many of the samples here have detailed considerations documented at the top of the code. Please read these carefully when importing the code.

Custom-check Name | Description | Author | Output
:---:| --- | --- | ---
[`ACE_security_openssl_version.js`](ACE_security_openssl_version.js) | Verify that your openssl version is up to date | Avantra Team | ![](./ACE_security_openssl_version.png)
[`ACE_security_auditd.js`](./ACE_security_auditD.js) | Monitor the output of the auditD search command | Avantra Team | ![](./ACE_security_auditd.png)