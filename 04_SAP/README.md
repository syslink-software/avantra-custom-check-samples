# SAP - Sample List #

This is the list of sample code provided in this section. This section is designed to showcase how to interact with your SAP system from a RUN_JS custom check.

Custom-check Name | Description | Author | Output
:---:| --- | --- | ---
[`ACE_sap_noOfWorkProcesses.js`](ACE_sap_noOfWorkProcesses.js) | Check the number of work processes in the SAP system | Avantra Team | ![](./ACE_sap_noOfWorkProcesses.png)
[`ACE_sap_sapStarIsLocked.js`](ACE_sap_sapStarIsLocked.js) | Verify that the SAP* user in a system is locked | Avantra Team | ![](./ACE_sap_sapStarIsLocked.png)