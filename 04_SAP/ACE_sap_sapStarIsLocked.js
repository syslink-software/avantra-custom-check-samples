// Name: Check that SAP* is locked in the system
// Type: Custom check example-code ONLY (please adapt to your environment)
// Author: Avantra SAP Engineering Team
// Date: 20th April 2021
// Description: A custom check that verifies that the SAP* user is locked.
//
// Considerations:
//                  - Valid for SAP systems
//
// Version Log:
//              1.0 - first release

const ccName = "ACE_sap_sapStarIsLocked"; // ace = Avantra, Custom Check, Example
const ccVersion = 1.0;

const searchUsername = "SAP*";
const userStatusInfo = {
    "L": {
        "key":"L",
        "information" : "Locked",
        "displayAs": OK
    },
    "U": {
        "key":"U",
        "information" : "Unlocked",
        "displayAs": CRITICAL
    },
    "UNKNOWN": {
        "key":"",
        "information" : "User not found",
        "displayAs": WARNING
    }
};

var userCount = 0;
var sapClientCount = 0;
var summaryMessages = '';
var errors = '';

// Function to turn the SAP user-info lock code into summary info we can use here.
// e.g. 'L' becomes 'Locked' and includes how we should display that info as defined
// in the constant userStatusInfo.
function getStatusInfo(statusCode) {
    if(statusCode == ""){
        return userStatusInfo["UNKNOWN"];
    } else {
        return userStatusInfo[statusCode];
    }
}

// Perform analysis of the given user on a specific client object.
function verifyUserComplianceForClient(connection, client, username){
    var func = connection.getFunction('BAPI_USER_GET_DETAIL');

    func.setImportParameter('USERNAME', username);

    // execute the function
    func.execute();

    // read Groups table – this is just for demo and display purposes
    var groupsTab = func.getTable('GROUPS');

    check.newRow();
    check.addCell(username);
    check.addCell(client);

    // groups
    var str = '';
    if (groupsTab.hasNext()) {
        str += group.getString('USERGROUP');
        while(groupsTab.hasNext()){
            var group = groupsTab.next();
            str += ', ' + group.getString('USERGROUP');
        }
    }
    check.addCell(str);
    
    
    var lockInformation = func.getExportStructure('ISLOCKED');
    var isLocked = getStatusInfo(lockInformation.getString('WRNG_LOGON'));
    var localLock = getStatusInfo(lockInformation.getString('LOCAL_LOCK'));
    var globalLock = getStatusInfo(lockInformation.getString('GLOB_LOCK'));
    var noPassword = getStatusInfo(lockInformation.getString('NO_USER_PW'));

    check.addCell(isLocked.information, isLocked.displayAs);
    check.addCell(localLock.information, localLock.displayAs);
    check.addCell(globalLock.information, globalLock.displayAs);
    check.addCell(noPassword.information, noPassword.displayAs);

    if(isLocked.displayAs === CRITICAL || isLocked.displayAs === CRITICAL || isLocked.displayAs === CRITICAL || isLocked.displayAs === CRITICAL){
        errors = errors + "- User " + username + " is not intentionally locked in client " + client + "\n";
    }
    userCount++;
}

// setup the summary table
check.addTableColumn('User');
check.addTableColumn('Client');
check.addTableColumn('Groups');
check.addTableColumn('Wrong Logons');
check.addTableColumn('Locally Locked');
check.addTableColumn('Globally Locked');
check.addTableColumn('No User or Password');

// 'sap' is the central object. It is hosted by the Avantra Agent and simply available in JavaScript.
// sap.clients = returns an array of clients of this SAP system
var clients = sap.clients;  // for example clients: 000,001,066,100
for (var i = 0; i < clients.length; i++) {
    var client = clients[i];

    try {
        // sap.login() does a login to the client given in the Avantra properties of the SAP system
        //             and uses the defined username and password
        // sap.login(client) uses the defined username and password for a given client
        var connection = sap.login(client);

        verifyUserComplianceForClient(connection, client,searchUsername);
        sapClientCount++;
    }
    catch (e) {
        errors += "- Unable to login to client " + client + " to perform checks - SKIPPING.\n";
    }
}

// write result status and message
var summaryMessages = '\nSummary:\n- ' + userCount + ' user(s) in ' + sapClientCount +' client(s) tested.\n';
if (errors !== '') {
    summaryMessages +=  '\nErrors:\n' + errors + '\n\n';
    check.status = CRITICAL;
}
else {
    summaryMessages += '- All users are locked.'
    check.status = OK;
}
check.message = summaryMessages;