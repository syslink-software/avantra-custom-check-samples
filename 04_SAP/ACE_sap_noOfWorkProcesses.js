// Name: Check the number of SAP work processes
// Type: Custom check example-code ONLY (please adapt to your environment)
// Author: Avantra SAP Engineering Team
// Date: 20th April 2021
// Description: A custom check that verifies the # of SAP work processes.
//
// Considerations:
//                  - Valid for SAP systems
//
// Version Log:
//              1.0 - first release

const ccName = "ACE_sap_noOfWorkProcesses"; // ace = Avantra, Custom Check, Example
const ccVersion = 1.0;

var func = rfcConnection.getFunction('TH_WPINFO');

// execute the function
func.execute();

// read WPLIST table
var table = func.getTable('WPLIST');

// prepare result table
check.addTableColumn('WP_INDEX');
check.addTableColumn('WP_PID');
check.addTableColumn('WP_ITYPE');
check.addTableColumn('WP_TYP');

var counter = 0;
while (table.hasNext()) {
    var entry = table.next();
    counter++;
    check.newRow();
    check.addCell(entry.getString('WP_INDEX'));
    check.addCell(entry.getString('WP_PID'));
    check.addCell(entry.getString('WP_ITYPE'));
    check.addCell(entry.getString('WP_TYP'));    
}
// make sure that custom monitoring parameter type cust.WorkProcessesWarnCount
// is created with a default value
var wpcc = Number(monitoredSystem.getMoniParam('cust.WorkProcessesWarnCount'));
if (wpcc && counter >= wpcc) {
    check.status = WARNING;
    check.message = counter + ' work processes found (more than limit of ' + wpcc + ')';
}else {
    check.status = OK;
    check.message = counter + ' work processes found';
}

// make sure custom performance counter Work Processes is created
performance.writePerformanceValue('Work Processes', counter);