# Avantra custom-check sample repository #

This repository contains a number of custom-check RUN_JS samples to guide you during your learning and creation of custom checks within [Avantra](https://www.avantra.com), the industry leading AIOps platform for SAP Operations. RUN_JS is used by Avantra customers to do many Advanced operations that are not already covered by Avantra's 160+ out of the box checks.

Within Avantra there are 40 types of custom checks available from targeting of processes at the OS level up to Idocs, Jobs, QRFC, audit logs at the SAP application level. The RUN_JS check, however, is the most powerful and versatile. It requires knowledge of Javascript and the ability to understand the examples contained in this repository.

## A note of caution ###
"With great power comes great responsibility". RUN_JS checks are both powerful and versatile and are capable of achieving a nearly infinite number of tasks within your managed landscape including damaging tasks. We encourage caution when utilizing this check type.

## What are custom checks? ###
Avantra allows you to develop your own custom "checks" which are performed periodically on servers, databases, SAP systems and more. The custom-check concept is very powerful and, when used correctly, has endeless possibilities for what is possible to achieve within your SAP Landscape.

RUN_JS custom-checks are written in Javascript and supports `ECMAScript 2019` for `Avantra Agents 20.11` and higher and `ECMAScript 5.1` for older versions.

Our formal documentation for RUN_JS custom-checks is available via our online documentation system at [docs.avantra.com](https://docs.avantra.com/product-guide/20.11/avantra/reference-custom-checks.html#run_js)

## What samples are included? ###

The samples are grouped according to their category. The categories are as follows:
* `01_Basics`
* `02_Server`
* `03_Security`
* `04_SAP`
* `05_Database`
* `06_Output_formatting`

Each sample file is named using the following template: `ACE_section_name.js` which stands for:
* `ACE` = Avantra custom-check example
* `section` = The section in which the sample lives e.g. Security or Basics
* `name` = The name of the sample
* `.js` = The file format to make it easier to open in a development enviornment


## How do I use these samples? ###

To utilize a sample, find the relevant folder and open the `.js` file included. Copy and paste the required portions of the sample into your Avantra system using the custom check type `Run_JS` (Only available in Avantra PRO or higher).

## Contribution guidelines ###

If you would like to submit a sample for inclusion we absolutely welcome this. Please fork this reporsitory, add your sample in the right area, and create a merge request. If you prefer, you can also send your sample to `support@avantra.com` and ask for it to be included by our team.

## Who do I talk to? ###

For questions and comments relating to this repository please use our forums on [support.avantra.com](support.avantra.com)

## Disclaimer

THIS SAMPLE CODE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE CODE OR THE USE OR OTHER DEALINGS IN THE CODE
